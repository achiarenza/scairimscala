﻿Imports Newtonsoft.Json.Linq
Imports ScairimCommons

Public Class Scala

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets a login. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pUser">            The user. </param>
    ''' <param name="pPass">            The pass. </param>
    ''' <param name="cUrlWebService">   The URL web service. </param>
    '''
    ''' <returns>   The login. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function GetLogin(pUser As String, pPass As String, cUrlWebService As String)
        Dim jsonTxt
        Dim oJsonW As JObject

        oJsonW = New JObject(
            New JProperty("username", pUser),
            New JProperty("password", pPass),
            New JProperty("networkId", 0),
            New JProperty("rememberMe", False))
        jsonTxt = oJsonW.ToString()
        oJsonW.RemoveAll()

        Dim headers(2, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsonTxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"

        GetLogin = Http.Send(jsonTxt, cUrlWebService & "/api/rest/auth/login", headers, "POST", "text")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets login sf. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pUser">            The user. </param>
    ''' <param name="pPass">            The pass. </param>
    ''' <param name="CallBack">         The call back. </param>
    ''' <param name="cUrlWebService">   The URL web service. </param>
    '''
    ''' <returns>   The login sf. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function GetLoginSF(pUser As String, pPass As String, CallBack As String, cUrlWebService As String)
        Dim jsonTxt
        Dim oJsonW As JObject

        oJsonW = New JObject(
            New JProperty("grant_type", "authorization_code"),
            New JProperty("client_id", pUser),
            New JProperty("client_id", pUser),
            New JProperty("redirect_uri", CallBack))
        jsonTxt = oJsonW.ToString()
        oJsonW.RemoveAll()

        Dim headers(2, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsonTxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"

        GetLoginSF = Http.Send(jsonTxt, cUrlWebService & "/services/oauth2/token", headers, "POST", "text")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets a logout. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pToken">           The token. </param>
    ''' <param name="pApiToken">        The API token. </param>
    ''' <param name="cUrlWebService">   The URL web service. </param>
    '''
    ''' <returns>   The logout. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function GetLogout(pToken As String, pApiToken As String, cUrlWebService As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        GetLogout = Http.Send("", cUrlWebService & "/api/rest/auth/logout/?" & "token=" & pToken, headers, "GET", "")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets the stuffs. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pToken">       The token. </param>
    ''' <param name="pApiToken">    The API token. </param>
    ''' <param name="pStuffs">      The stuffs. </param>
    '''
    ''' <returns>   The stuffs. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function GetStuffs(pToken As String, pApiToken As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        GetStuffs = Http.Send("", pStuffs, headers, "GET", "text")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets stuffs bearer. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="bearerToken">  The bearer token. </param>
    ''' <param name="pStuffs">      The stuffs. </param>
    '''
    ''' <returns>   The stuffs bearer. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function GetStuffsBearer(bearerToken As String, pStuffs As String)
        Dim headers(1, 1)
        headers(0, 0) = "Authorization"
        headers(0, 1) = "Bearer " & bearerToken
        headers(1, 0) = "Content-Type"
        headers(1, 1) = "application/json"
        GetStuffsBearer = Http.Send("", pStuffs, headers, "GET", "text")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Creates a player. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pToken">       The token. </param>
    ''' <param name="pApiToken">    The API token. </param>
    ''' <param name="jsontxt">      The jsontxt. </param>
    ''' <param name="url">          URL of the resource. </param>
    '''
    ''' <returns>   The new player. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function CreatePlayer(pToken As String, pApiToken As String, jsontxt As String, url As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsontxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        CreatePlayer = Http.Send(jsontxt, url & "/api/rest/players", headers, "POST", "text")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Updates the stuffs. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pToken">       The token. </param>
    ''' <param name="pApiToken">    The API token. </param>
    ''' <param name="jsontxt">      The jsontxt. </param>
    ''' <param name="pStuffs">      The stuffs. </param>
    '''
    ''' <returns>   . </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function UpdateStuffs(pToken As String, pApiToken As String, jsontxt As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsontxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        UpdateStuffs = Http.Send(jsontxt, pStuffs, headers, "PUT", "text")
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Deletes the stuffs. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="pToken">       The token. </param>
    ''' <param name="pApiToken">    The API token. </param>
    ''' <param name="pStuffs">      The stuffs. </param>
    '''
    ''' <returns>   . </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function DeleteStuffs(pToken As String, pApiToken As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        DeleteStuffs = Http.Send("", pStuffs, headers, "DELETE", "status", 10000)
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Queries if a given player exists. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="PlayerName">   Name of the player. </param>
    ''' <param name="LinkCM">       The link centimetres. </param>
    '''
    ''' <returns>   . </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function PlayerExists(PlayerName As String, LinkCM As String, token As String, apiToken As String)
        If GetPlayerId(PlayerName, LinkCM, token, apiToken) = "" Then
            PlayerExists = False
        Else
            PlayerExists = True
        End If
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets player identifier. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="PlayerName">   Name of the player. </param>
    ''' <param name="LinkCM">       The link centimetres. </param>
    '''
    ''' <returns>   The player identifier. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Function GetPlayerId(PlayerName As String, LinkCM As String, token As String, apiToken As String)
        Dim oJsonR As JObject
        Dim result = GetStuffs(token, apiToken, LinkCM & "/api/rest/players?search=" & PlayerName)
        If InStr(result, "ERROR") > 0 Then
            GetPlayerId = "ERROR - GetPlayerId: Ricerca Player sul CM fallita. Probabili problemi di Login."
        Else
            oJsonR = JObject.Parse(result)
            If oJsonR IsNot Nothing Then
                If oJsonR.ContainsKey("list") Then
                    GetPlayerId = ""
                    For Each child In oJsonR.Item("list").Children()
                        If child.Item("name").ToString = PlayerName Then
                            GetPlayerId = child.Item("id").ToString
                        End If
                    Next
                Else
                    GetPlayerId = ""
                End If
            Else
                GetPlayerId = "ERROR - GetPlayerId: Parsing JSON Fallito."
            End If
        End If
    End Function

    Function GetSession(pToken As String, pApiToken As String, url As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken

        GetSession = Http.Send("", url & "/api/rest/auth/get", headers, "GET", "")
    End Function
End Class
